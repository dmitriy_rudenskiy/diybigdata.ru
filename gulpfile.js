// Include gulp
var gulp = require('gulp');

// Include Our Plugins
// var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var swig = require('gulp-swig');
var clean = require('gulp-clean');
var concatCss = require('gulp-concat-css');
var cssmin = require('gulp-cssmin');
var inject = require('gulp-inject');

gulp.task('clear', function () {
    return gulp.src(['public/css/*', 'public/*.html'], {read: false})
        .pipe(clean());
});

gulp.task('css', function() {
    return gulp.src('src/css/*.css')
        .pipe(concatCss("css/style.css"))
        .pipe(cssmin())
        .pipe(gulp.dest('public/'));
});

gulp.task('templates', function() {

    var opts = {
        data: {
            title: "Тут название сайта!"
        }
    };

    gulp.src(['views/*.twig', '!views/layout.twig'])
        .pipe(swig(opts))
        .pipe(gulp.dest('public/'))
});

/*
// Lint Task
gulp.task('lint', function() {
    return gulp.src('js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src('scss/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('dist/css'));
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src('js/*.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('dist'))
        .pipe(rename('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'));
});



// Default Task
gulp.task('default', ['lint', 'sass', 'scripts', 'watch']);
    */

// Watch Files For Changes
gulp.task('watch', function() {
    // gulp.watch('src/js/*.js', ['lint', 'scripts']);
    gulp.watch('src/css/*.css', ['build']);
    gulp.watch('views/*.twig', ['build']);
});

gulp.task('build', ['clear', 'css', 'templates']);

// Default Task
gulp.task('default', ['build', 'watch']);