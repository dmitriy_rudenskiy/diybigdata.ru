var PostModel = require('./../models/post');

module.exports = {
    paginate: function (req, res) {

        var page = (req.query.page || req.params.page || 1) * 1;

        if (page < 1) {
            page = 1;
        }

        PostModel.paginate({}, {page: page, limit: 2}, function (err, paginate) {
            if (!err) {
                res.render('api', {
                    title: 'Superhero API',
                    paginate: paginate
                });
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s', res.statusCode, err.message);
                res.send({error: 'Server error'});
            }
        });
    },
    insert: function (req, res) {

        var post = new PostModel({
            title: req.body.name,
            content: 'Test'
        });

        post.save(function (err) {
            if (err) {
                throw err;
            }
        });

        res.redirect('/api');
    }
};






