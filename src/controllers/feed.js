var Feed = require('feed');

module.exports = {
    index: function (req, res) {

        var feed = new Feed({
            id: 'My work!',
            title: 'Feed Title',
            description: 'This is my personnal feed!',
            link: 'http://example.com/',
            image: 'http://example.com/image.png',
            copyright: 'All rights reserved 2013, John Doe',
            updated: new Date(2013, 06, 14),
            author: {
                name: 'John Doe',
                email: 'johndoe@example.com',
                link: 'https://example.com/johndoe'
            }
        });

        // <atom:link href="http://dallas.example.com/rss.xml" rel="self" type="application/rss+xml" />

        // добавляем новости
        /*
        for(var key in posts) {
            feed.addItem({
                title:          posts[key].title,
                link:           posts[key].url,
                description:    posts[key].description,
                author: [
                    {
                        name:   'Jane Doe',
                        email:  'janedoe@example.com',
                        link:   'https://example.com/janedoe'
                    },
                    {
                        name:   'Joe Smith',
                        email:  'joesmith@example.com',
                        link:   'https://example.com/joesmith'
                    }
                ],
                contributor: [
                    {
                        name:   'Shawn Kemp',
                        email:  'shawnkemp@example.com',
                        link:   'https://example.com/shawnkemp'
                    },
                    {
                        name:   'Reggie Miller',
                        email:  'reggiemiller@example.com',
                        link:   'https://example.com/reggiemiller'
                    }
                ],
                date:           posts[key].date,
                image:          posts[key].image
            });
        }
        */


        feed.addCategory('Technologie');

        feed.addContributor({
            name: 'Johan Cruyff',
            email: 'johancruyff@example.com',
            link: 'https://example.com/johancruyff'
        });

        var xml = feed.render('rss-2.0');

        // var xml = feed.render('atom-1.0');


        res.send(xml);
    }
};