const apiController = require('./controllers/api');
const feedController = require('./controllers/feed');

module.exports = function (router) {
    router.get('/api/:page', apiController.paginate);
    router.get('/api', apiController.paginate);
    router.post('/api', apiController.insert);

    /* GET home page. */
    router.get('/', function(req, res, next) {
        res.render('index', { title: 'Express' });
    });

    // Express get listener event for url to RSS feed
    router.get('/feed', feedController.index);
    router.get('/feed/atom', feedController.index);
    router.get('/feed/rss', feedController.index);

    return router;
};