var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TaskSchema = Schema({
    todo: {
        type: String
    },
    completed: {
        type: Boolean,
        default: false
    },
    created_by: {
        type: Date,
        default: Date.now
    }
});


var TaskModel = mongoose.model('Task', TaskSchema);

module.exports = TaskModel;