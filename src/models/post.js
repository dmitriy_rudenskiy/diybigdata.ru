const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

// create a schema
var PostSchema = new Schema({
    title: String,
    content: String
});

//
PostSchema.plugin(mongoosePaginate);

// on every save, add the date
PostSchema.pre('save', function (next) {

    // get the current date
    var currentDate = new Date();

    // change the updated_at field to current date
    this.updated_at = currentDate;

    // if created_at doesn't exist, add to that field
    if (!this.created_at) {
        this.created_at = currentDate;
    }

    next();
});

// make this available to our users in our Node applications
module.exports = mongoose.model('Post', PostSchema);
