require('dotenv').config();

const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const swig = require('swig');
const port = (process.env.PORT || 3000);

// fix
swig.setDefaults({ locals: {
    range: function (start, len) {
        return (new Array(len)).join().split(',').map(function (n, idx) { return idx + start; });
    }
}});

// config
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, './../../public')));

// view engine setup
app.set('views', path.join(__dirname, './../views'));
app.engine('twig', swig.renderFile);
app.set('view engine', 'twig');

// routers
app.use(require('./../routes')(express.Router()));

// handling 404 errors
app.use(function(req, res, next) {

    if (process.env.NODE_ENV !== 'prod') {
        console.log("404\t" + req.originalUrl);
    }

    res.status(404).sendFile(path.join(__dirname, './../../public/error/404.html'));
});

// Errors
app.use(function(err, req, res, next) {

    if (process.env.NODE_ENV !== 'prod') {
        console.log(err.stack);
    }

    res.status(err.status || 404);

    if (err.status !== 404) {
        console.log(err.status + "\t" + req.originalUrl);
    }
});

// Connect to Mongo on start
mongoose.Promise = require('bluebird');
mongoose.connect(process.env.DB_URL, function (err) {
    if (err) {
        console.log('Error: ' + err);
        process.exit(1);
    }
});

var listener = app.listen(port, function () {
    // console.log('Server listening on port ' + listener.address().port);
});

module.exports = app;